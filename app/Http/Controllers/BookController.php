<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;



use App\Book;
use App\User;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all books
      //$books = Book::all();
      //  $id = Auth::id();
        //$books= User::find($id)->books;
      //return view('books.index', ['books' => $books]);
      $id = Auth::id();
      if (Gate::denies('manager')) {
          $boss = DB::table('employees')->where('employee',$id)->first();
          $id = $boss->manager;
      }
      $user = User::find($id);
      $books = $user->books;
      return view('books.index', compact('books'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create book!");
        }
          return view ('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $books = new Book(); 
        $books->title = $request->title;
        $books->author = $request->author;
        $books->user_id = Auth::id();
        $books->created_at = null;
        $books->updated_at = null;
        $books->status = 0;
        $books->save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"you cant updat!");
       }
        $book = Book::find($id);
        return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You cant update!");
        }   
       // לא עובד if(!$book->user->id == Auth::id()) return(redirect('books'));
        //test if title is dirty
       
        if ($book->status == 0 ){
        $book->update($request->except(['_token']));
   
        if($request->ajax()){
            return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
        }}
         else {   
               
            return redirect('books');           
        }
    
        //$book ->update($request->all());
        //return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"you cant updat!");
       }
        $book = Book::find($id);
      //  if(!$book->user->id == Auth::id()) return(redirect('books'));
      if(!$book->user_id == Auth::id()) return(redirect('books'));
        $book->delete();
        return redirect('books');
    }
}
