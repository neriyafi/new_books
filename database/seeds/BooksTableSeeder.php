<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                    'title' => 'Oliver Twist',
                    'author' => 'Charles Dickens' ,
                    'user_id' => 1,
                    'status' => 1,
                ],
                [
                    
                    'title' => 'Romeo and Juliet',
                    'author' => 'William Shakespear' ,
                    'user_id' => 1,
                    'status' => 1,
                ],
                [
                    
                    'title' => 'Harry Potter',
                    'author' => 'J.K Rowling' ,
                    'user_id' => 2,
                    'status' => 1,
                ],
                [
                   
                    'title' => 'White fang',
                    'author' => 'Jeck London' ,
                    'user_id' => 2,
                    'status' => 1,
                ],
                [
                    
                    'title' => 'Little Women',
                    'author' => 'Louisa Alcott' ,
                    'user_id' => 3,
                    'status' => 1,
                ],
            ]);
    }
}
