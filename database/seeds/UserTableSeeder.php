<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'ner',
                    'email' => 'ner@gmail.com',
                    'password'=> '12345',
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'name' => 'nad',
                    'email' => 'nad@gmail.com',
                    'password'=> '12345',
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'name' => 'as',
                    'email' => 'as@gmail.com',
                    'password'=> '12345678',
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'employee' ,
                ],
                [
                    'name' => 'rrr',
                    'email' => 'rrr@gmail.com',
                    'password'=> '12345678',
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'employee' ,
                ],
            ]);
    }
    }

