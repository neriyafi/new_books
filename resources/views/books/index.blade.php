
<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            
          }

          tr:nth-child(even) {
            background-color: #dddddd;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
      
          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }
</style>
</head>

@extends('layouts.app')
@section('content')

<body>
          <table>
            <tr>
              <th>ID</th>
              @can('manager')  <th>Title</th> @endcan
              @cannot('manager') <th>Title</th> @endcannot
              <th>Author</th>
              <th>user_id</th>
              <th> read </th>
            </tr>
                    



            @foreach($books as $book)
            <tr>
             <td>{{$book->id}}</td>
             @can('manager')    <td> <a href = "{{route('books.edit', $book->id)}}" >  {{$book->title}} </a> </td>  @endcan
             @cannot('manager')  <td> {{$book->title}} </td>  @endcannot
             <td>{{$book->author}}</td>
             <td>{{$book->user_id}}</td>
              <td>           @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif   </td>
            </tr>
            
            @endforeach
           </table>

      @can('manager')

           <a href = "{{route('books.create')}}"> create a new Book </a>

           @endcan

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
             console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function(data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  

</body>
   
@endsection